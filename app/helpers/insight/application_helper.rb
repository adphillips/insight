module Insight
  module ApplicationHelper

    def colored_change value, precision=2
      return '' if value.nil? or value.nan?

      color_class = (value < 0) ? 'downward_change' : 'upward_change'
      "<span class=\"#{color_class}\">#{value.round(precision)}</span>".html_safe if value
    end
  end
end

