module Insight
  class DashboardController < ApplicationController

    def home
      @experiments = Insight::Experiment.all
    end

  end
end
