module Insight
  class Sample < ActiveRecord::Base

    belongs_to :experiment, :class_name => 'Insight::Experiment'

    serialize :data

  end
end
