module Insight
  # One of the possible alternative values for the test
  class Alternative

    include Insight::Statistics::Alternative

    attr_accessor :experiment_uid, :title, :uid

    def initialize uid, &block
      @uid = uid
      @title ||= uid.to_s.humanize
      instance_eval &block if block_given?
    end

    # Whether this alternative is a control for the experiment
    def control?
      (uid && experiment.control && experiment.control.uid) ? (uid == experiment.control.uid) : false
    end

    # Number of times the alternative has been converted
    def conversions
      Insight.cache.read(cache_key_for(:conversions)) || 0
    end

    # Sets number of times the alternative has been converted
    def conversions= count
      Insight.cache.write cache_key_for(:conversions), count
    end

    # The experiment for the alternative
    def experiment
      @experiment
    end

    # Sets the experiment for the alternative
    def experiment= experiment
      @experiment = experiment
      @experiment_uid = experiment.uid
    end

    # Boolean as to whether the alternative is the leader in the experiment
    def leader?
      (self == experiment.leader)
    end

    # Number of participants shown the alternative
    def participants
      Insight.cache.read(cache_key_for(:participants)) || 0
    end

    # Sets number of participants shown the alternative
    def participants= count
      Insight.cache.write cache_key_for(:participants), count
    end

    # Sets the title if a parameter is passed
    # Returns the title
    def title value=nil
      @title = value unless value.nil?
      @title
    end

    private

    # Generates a cache key based on the alternative and experiment
    def cache_key_for identifier
      identifier = [identifier] unless identifier.kind_of?(Array)
      identifier = [:alternatives, uid] + identifier
      experiment.cache_key_for identifier
    end

  end
end
