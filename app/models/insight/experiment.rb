require 'insight/sampler'

module Insight
  class Experiment < ActiveRecord::Base

    include Insight::Sampler

    attr_accessor :alternatives, :control, :description, :title

    after_initialize do
      if self.uid
        self.uid = self.uid.to_sym
        self.title ||= self.uid.to_s.humanize
        load_definition
      end
    end

    # Returns the specified alternative or the control alternative if :control is passed
    def alternative uid
      return control if uid.to_sym == :control
      alternatives[uid.to_sym]
    end

    # Returns the chosen alternative for this experiement for the participant with the specified identifier or nil if the identifier isn't in the experiment
    def alternative_for identifier
      cache_id = [identifier, :participant]
      cache_value = Insight.cache.read(cache_key_for(cache_id))
      return alternative(cache_value) unless cache_value.nil? || cache_value.blank?
    end

    # If an array, will be concatenated using / as a separator
    def cache_key_for identifier
      identifier = [identifier] unless identifier.kind_of?(Array)
      Insight.cache_key_for ['split_tests', uid.to_s] + identifier
    end

    # Marks the specified participant as converted
    def convert identifier
      return unless participant?(identifier)
      return if converted?(identifier)

      @conversions ||= 0
      @conversions += 1
      Insight.cache.write conversions_cache_key, @conversions
      Insight.cache.write cache_key_for([identifier, :converted]), true

      alternative_for(identifier).conversions += 1
    end

    # Number of conversions for the experiment
    def conversions
      @conversions ||= Insight.cache.read(conversions_cache_key)
      if @conversions.nil?
        @conversions = 0
        @conversions = alternatives.inject(0) {|sum, alt| sum + alt[1].conversions} if alternatives
        @conversions += control.conversions if control
      end
      @conversions
    end

    # Whether the specified participant has converted
    def converted? identifier
      Insight.cache.read(cache_key_for([identifier, :converted])) || false
    end

    # Returns the definition block for the experiment
    def definition
      @definition ||= Insight::Experiment.definitions[self.uid] if Insight::Experiment.definitions[self.uid]
    end

    # Returns the path to the experiment definition
    def definition_path
      @definition_path ||= "#{Rails.root.to_s}/#{Insight::EXPERIMENT_FOLDER}/#{self.uid}.rb"
    end

    # Whether the experiment has a record in the database
    def has_record?
      !!(Insight::Experiment.where(:uid => self.uid).count > 0)
    end

    # Marks the specified id as a participant in the experiment
    def join identifier, force_choice=nil
      cache_id = [identifier, :participant]

      return alternative_for(identifier) if participant? identifier
      next_alt = (force_choice) ? force_choice : next_alternative
      choice = alternative(next_alt)

      Insight.cache.write(cache_key_for(cache_id), next_alt)
      self.participants += 1
      self.save

      choice.participants = choice.participants + 1 if choice
      sample if sample_required?

      choice
    end

    def leader
      leader = control

      alternatives.each do |akey, a|
        leader = a if a.conversion_rate > leader.conversion_rate
      end

      leader
    end

    # Removes the participant from the experiment.
    # Does not update the conversion or participant counts in any way
    def leave identifier
      cache_id = [identifier, :participant]
      cache_key = cache_key_for cache_id
      Insight.cache.delete(cache_key)
    end

    # Loads the definition from the file specified in definition path unless it has already been loaded.
    def load_definition
      if definition
        Insight::Dsl::ExperimentDsl.new self, &definition if definition
        Insight::Experiment.defined[self.uid] = self
      else
        load_definition! unless definition
      end
    end

    # Loads the definition and overwrites if already loaded
    def load_definition!
      load definition_path if File.exists? definition_path
      Insight::Dsl::ExperimentDsl.new self, &definition if definition
      Insight::Experiment.defined[self.uid] = self
    end

    # If there's an existing record with the specified uid, its id is loaded
    # Returns boolean as to wether a record was found
    def load_id
      self.id = Insight::Experiment.where(:uid => self.uid).first.id if self.has_record?
    end

    # Returns the next alternative
    # TODO currently only works for single alternative.  need to improve this
    def next_alternative
      return :control unless control and alternatives

      if control.participants > alternatives.first[1].participants
        alternatives.first[0]
      else
        :control
      end
    end

    # Returns boolean as to whether the specified identifier is a participant in the test
    def participant? identifier
      cache_id = [identifier, :participant]
      cache_key = cache_key_for cache_id
      !Insight.cache.read(cache_key).nil?
    end

    def participants
      result = 0
      result = alternatives.inject(0) {|sum, alt| sum + alt[1].participants} if alternatives
      result += control.participants if control
      result
    end

    cattr_reader :definitions
    cattr_accessor :defined

    # Class methods
    class << self

      # Defines a new experiment using the ExperimentDsl
      def define test_uid, &block
        @defined ||= {}
        @definitions ||= {}
        @definitions[test_uid] = block

        e = find_or_create_by_uid test_uid

        return e
      end

      # Defined experiment records
      def defined
        @defined ||= {}
      end

      # Experiment definition blocks
      def definitions
        @definitions ||= {}
      end

      # Destroy the specified experiment by deleting record and un-defining
      def destroy_by_uid uid
        e = find_by_uid uid if exists?(:uid => uid)
        e.delete if e
        undefine uid
      end

      # Returns the experiment record if it has one or creates one if not
      def find_or_create_by_uid uid
        uid = uid.to_sym
        return @defined[uid] unless @defined.nil? or @defined[uid].nil?

        experiment = super uid

        @defined[uid] = experiment
      end

      # Removes the definition of the experiment
      def undefine uid
        @defined[uid] = nil
        @definitions[uid] = nil
      end

    end

    private

    # Cache key for storing conversion counts
    def conversions_cache_key
      cache_key_for :conversions
    end

    # Cache key for storing participant counts
    def participants_cache_key
      cache_key_for :participants
    end
  end
end
