Insight
=======

[![Code Climate](https://codeclimate.com/badge.png)](https://codeclimate.com/github/adamphillips/insight)

Insight is (or will be) a Rails engine for running A/B and split test experiments
