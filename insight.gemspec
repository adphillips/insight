$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "insight/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "insight"
  s.version     = Insight::VERSION
  s.authors     = ["Adam Phillips"]
  s.email       = ["adam@29ways.co.uk"]
  s.homepage    = "https://github.com/adamphillips/insight"
  s.summary     = "Split testing framework"
  s.description = "Split testing framework"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 3.1.0"
  s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'factory_girl', '1.3.3'
  s.add_development_dependency 'ruby-debug'
  s.add_development_dependency 'shoulda'
  s.add_development_dependency 'mocha'
  s.add_development_dependency 'test_console'
  s.add_development_dependency 'ansi'
  s.add_development_dependency 'facets'
  s.add_development_dependency 'turn'
end
