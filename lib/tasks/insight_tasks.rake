# desc "Explaining what the task does"
# task :insight do
#   # Task goes here
# end

namespace :insight do
  desc 'Load experiment definitions'
  task :load_definitions => :environment do
    Dir["#{File.join([Rails.root.to_s, 'app', 'experiments'])}/**/*.rb"].each { |f| puts "#{f}";  load f }
  end

  desc 'Runs the specified test a number of times'
  task :run => :load_definitions do
    STDOUT.puts 'Enter the experiment uid'
    test_uid = STDIN.gets.chomp
    @experiment = Insight::Experiment.find_or_create_by_uid test_uid.to_sym
    @experiment.load_definition!

    if @experiment.control
      STDOUT.puts 'Enter the number of participants in the control group'
      control_participants = STDIN.gets.chomp.to_i

      STDOUT.puts 'Enter the conversion rate in the control group (eg 0.76)'
      control_rate = STDIN.gets.chomp.to_f

      for i in 1..control_participants
        id = "control_#{i}_#{Time.now.to_i}"
        @experiment.join id, :control
        if rand <= control_rate
          @experiment.convert id
        end
      end
    end

    @experiment.alternatives.each do |akey, a|
      STDOUT.puts "Enter the number of participants in alternative #{akey.to_s}"
      a_participants = STDIN.gets.chomp.to_i

      STDOUT.puts "Enter the conversion rate in alternative #{akey.to_s} (eg 0.76)"
      a_rate = STDIN.gets.chomp.to_f

      for i in 1..a_participants
        id = "#{akey}_#{i}_#{Time.now.to_i}"
        @experiment.join id, a.uid
        if rand <= a_rate
          @experiment.convert id
        end
      end
    end if @experiment.alternatives
  end

  desc 'Set conversion data'
  task :set_data => :load_definitions do
    STDOUT.puts 'Enter the experiment uid'
    test_uid = STDIN.gets.chomp
    @experiment = Insight::Experiment.find_or_create_by_uid test_uid.to_sym
    @experiment.load_definition!

    if @experiment.control
      STDOUT.puts 'Enter the number of participants in the control group'
      @experiment.control.participants = STDIN.gets.chomp.to_i

      STDOUT.puts 'Enter the number of conversions in the control group'
      @experiment.control.conversions = STDIN.gets.chomp.to_i
    end

    @experiment.alternatives.each do |akey, a|
      STDOUT.puts "Enter the number of participants in alternative #{akey.to_s}"
      a.participants = STDIN.gets.chomp.to_i

      STDOUT.puts "Enter the number of conversions in alternative #{akey.to_s}"
      a.conversions = STDIN.gets.chomp.to_i
    end if @experiment.alternatives
  end
end
