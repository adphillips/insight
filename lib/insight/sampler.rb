module Insight
  # Functions for sampling data from an experiment
  module Sampler

    extend ActiveSupport::Concern

    included do
      has_many :samples
    end

    def sample
      data = {:participants => participants, :conversions => conversions, :alternatives => {}}

      data[:alternatives][:control] = {:participants => control.participants, :conversions => control.conversions}
      alternatives.each do |akey, aval|
        data[:alternatives][akey] = {:participants => aval.participants, :conversions => aval.conversions}
      end

      Insight::Sample.create(:experiment => self, :data => data)
    end

    def sample_rate
      @sample_rate ||= {:participants => 0, :conversions => 0}
    end

    def sample_required?
      if sample_rate[:participants] > 0 && self.participants.modulo(self.sample_rate[:participants]) == 0
        return true
      end

      false
    end
  end
end
