module Insight
  module Helpers
    module ControllerHelpers

      # Returns the alternative for the specified test
      #   @param uid Symbol uid of the test
      #   @return Insight::Alternative
      def alternative_for uid
        experiment = Insight::Experiment.find_or_create_by_uid uid
        experiment.alternative_for insight_id
      end

      # Converts the specified test(s)
      #  @param test_ids Can be a symbol or array of symbols
      def convert! test_ids
        test_ids = [test_ids] unless test_ids.kind_of? Array
        test_ids.each do |uid|
          experiment = Insight::Experiment.find_or_create_by_uid uid
          experiment.convert insight_id
        end
      end

      # Runs a block of code passing the chosen alternative for the specified test
      # If a hash is passed of the form test => alternative the block is only executed if the chosen alternative matches the specified one.
      # Otherwise a symbol should be passed to identity the test
      #
      #   @example
      #   for_test :a_test do |alternative|
      #     case alternative.uid
      #       when :control
      #       when :the_alternative
      #         ...
      #       end
      #   end
      #
      #   for_test :a_test => :control do
      #     # Execute control code
      #   end
      #
      #   for test :a_test => :the_alternative di
      #     # Execute code for the alternative
      #   end
      def for_test opts, &block
        test_id = opts if opts.kind_of? Symbol

        if opts.kind_of? Symbol
          test_id = opts
        else
          test_id, alternative_id = opts.first
        end

        experiment = Insight::Experiment.find_by_uid test_id
        raise "Experiment not found #{test_id}" unless experiment
        experiment.join insight_id

        if alternative_id
          alternative = alternative_for(test_id)

          yield(alternative) if alternative_id == alternative.uid
        elsif test_id
          alternative = alternative_for(test_id)
          yield(alternative)
        end
      end

      # Returns the insight id used for the request.
      # Default to the session id
      def insight_id
        request.session_options[:id]
      end

      def split_test uid, &block
        experiment = Insight::Experiment.find_or_create_by_uid uid
        experiment.join insight_id
        @split_tests ||= {}
        @split_tests[uid] = experiment.alternative_for insight_id
      end
    end
  end
end
