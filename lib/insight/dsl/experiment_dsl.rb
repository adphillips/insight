module Insight
  module Dsl
    # Experiment DSL
    #  Defines the DSL for creating experiments
    #
    #  Insight::Experiment.define do
    #  end
    class ExperimentDsl

      def initialize target, &block
        @target = target
        instance_eval &block if block_given?
      end

      # Specifies a new alternative for the experiment
      def alternative uid, &block
        @target.alternatives ||= {}

        alternative = @target.alternatives[uid]
        alternative ||= Insight::Alternative.new uid
        alternative.experiment = @target
        alternative.instance_eval(&block)

        @target.alternatives[uid] = alternative
      end

      # Specifies the control group for the experiment
      def control &block
        @target.control ||= Insight::Alternative.new :control
        @target.control.experiment = @target
        @target.control.instance_eval(&block)
      end

      # Specifies a description for the experiment
      def description value
        @target.description = value
      end

      # Specifies a title for the experiment
      def title value
        @target.title = value
      end
    end
  end
end
