require 'insight/sampler'
require 'insight/statistics/alternative'
require 'insight/helpers/controller_helpers'

module Insight
  class Engine < Rails::Engine
    isolate_namespace Insight
  end
end

class ActionController::Base
  include Insight::Helpers::ControllerHelpers
end
