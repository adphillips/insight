module Insight
  module Statistics
    module Alternative
      def confidence_level
        return 0.0 if control?

        z_max = 6

        begin
          z = z_score.abs
        rescue
          return 0.0
        end

        if (z == 0.0)
          x = 0.0;
        else
          y = 0.5 * z.abs
          if (y > (z_max * 0.5))
            x = 0.9999
          elsif (y < 1.0)
            w = y * y
            x = ((((((((0.000124818987 * w
                - 0.001075204047) * w + 0.005198775019) * w
                - 0.019198292004) * w + 0.059054035642) * w
                - 0.151968751364) * w + 0.319152932694) * w
                - 0.531923007300) * w + 0.797884560593) * y * 2.0
          else
            y -= 2.0
            x = (((((((((((((-0.000045255659 * y
                + 0.000152529290) * y - 0.000019538132) * y
                - 0.000676904986) * y + 0.001390604284) * y
                - 0.000794620820) * y - 0.002034254874) * y
                + 0.006549791214) * y - 0.010557625006) * y
                + 0.011630447319) * y - 0.009279453341) * y
                + 0.005353579108) * y - 0.002141268741) * y
                + 0.000535310849) * y + 0.999936657524
          end
        end

        p = (z > 0.0) ? ((x + 1.0) * 0.5) : ((1.0 - x) * 0.5)
        # p.round(5) unless p.nil? or p.nan?
      end

      def conversion_change
        return 0.0 if control?
        return 0.0 unless experiment.control.conversion_rate > 0
        ((conversion_rate - experiment.control.conversion_rate) / experiment.control.conversion_rate)
      end

      def conversion_rate
        (participants.to_i > 0) ? (conversions / participants.to_f).round(2) : 0
      end

      def z_score
        return 0.0 if control?
        return 0.0 unless participants > 0 and experiment.control.participants > 0

        cr1 = conversion_rate
        cr2 = experiment.control.conversion_rate

        n1 = participants
        n2 = experiment.control.participants

        numerator = cr1 - cr2
        frac1 = cr1 * (1 - cr1) / n1
        frac2 = cr2 * (1 - cr2) / n2

        numerator / ((frac1 + frac2) ** 0.5)
      end
    end
  end
end
