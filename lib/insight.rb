require 'insight/engine'
require 'insight/version'

require 'insight/dsl/experiment_dsl'


module Insight

  EXPERIMENT_FOLDER = 'app/experiments'

  class << self
    def cache
      @cache ||= Rails.cache
    end

    def cache= cache_store
      @cache = cache_store
    end

    def cache_key_for identifier
      identifier = identifier.join('/') if identifier.kind_of?(Array)
      identifier.to_s
    end
  end
end
