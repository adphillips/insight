class AddInsightDefaults < ActiveRecord::Migration
  def up
    change_column :insight_experiments, :control_participants, :integer, :default => 0
    change_column :insight_experiments, :control_conversions, :integer, :default => 0
    change_column :insight_experiments, :alternative_participants, :integer, :default => 0
    change_column :insight_experiments, :alternative_participants, :integer, :default => 0

    change_column :insight_samples, :control_participants, :integer, :default => 0
    change_column :insight_samples, :control_conversions, :integer, :default => 0
    change_column :insight_samples, :control_conversion_rate, :integer, :default => 0
    change_column :insight_samples, :alternative_participants, :integer, :default => 0
    change_column :insight_samples, :alternative_conversions, :integer, :default => 0
    change_column :insight_samples, :alternative_conversion_rate, :decimal, :default => 0

    change_column :insight_samples, :z_score, :decimal, :default => 0
    change_column :insight_samples, :p_value, :decimal, :default => 0
  end

  def down
  end
end
