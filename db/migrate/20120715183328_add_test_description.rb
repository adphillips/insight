class AddTestDescription < ActiveRecord::Migration
  def up
    add_column :insight_experiments, :description, :text
  end

  def down
  end
end
