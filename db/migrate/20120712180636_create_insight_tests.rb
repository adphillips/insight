class CreateInsightTests < ActiveRecord::Migration
  def change
    create_table :insight_experiments do |t|
      t.string :uid
      t.string :title

      t.integer :control_participants
      t.integer :control_conversions

      t.integer :alternative_participants
      t.integer :alternative_conversions

      t.boolean :started, :default => false
      t.boolean :complete, :default => false

      t.datetime :completed_at

      t.timestamps
    end
  end
end
