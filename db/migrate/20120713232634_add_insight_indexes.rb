class AddInsightIndexes < ActiveRecord::Migration
  def up
    add_index :insight_experiments, :uid, :unique
    add_index :insight_experiments, :started
    add_index :insight_experiments, :complete
    add_index :insight_experiments, :completed_at

    rename_column :insight_samples, :insight_test_id, :insight_experiment_id

    add_index :insight_samples, :insight_experiment_id
    add_index :insight_samples, :p_value
  end

  def down
  end
end
