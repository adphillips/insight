class AddParticipantCount < ActiveRecord::Migration
  def up
    add_column :insight_experiments, :participants, :integer, :default => 0
    add_column :insight_samples, :participants, :integer, :default => 0
  end

  def down
  end
end
