class RenameSampleExperiment < ActiveRecord::Migration
  def up
    rename_column :insight_samples, :insight_experiment_id, :experiment_id
  end

  def down
    rename_column :insight_samples, :experiment_id, :insight_experiment_id
  end
end
