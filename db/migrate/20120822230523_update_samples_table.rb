class UpdateSamplesTable < ActiveRecord::Migration
  def up
    add_column :insight_samples, :data, :text
  end

  def down
  end
end
