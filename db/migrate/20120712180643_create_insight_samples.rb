class CreateInsightSamples < ActiveRecord::Migration
  def change
    create_table :insight_samples do |t|
      t.integer :insight_test_id

      t.integer :control_participants
      t.integer :control_conversions
      t.decimal :control_conversion_rate

      t.integer :alternative_participants
      t.integer :alternative_conversions
      t.decimal :alternative_conversion_rate

      t.decimal :z_score
      t.decimal :p_value

      t.timestamps
    end
  end
end
