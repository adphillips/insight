Factory.define :experiment, :class => 'Insight::Experiment' do |f|
  f.title 'An AB test'
  f.uid :a_simple_test
end

Factory.define :experiment_with_alternatives, :class => 'Insight::Experiment' do |f|
  f.title 'A split test'
  f.uid :a_split_test

  f.after_build do |e|
    Insight::Experiment.define e.uid do
      #out self, :blue
      control do
      end

      alternative :alt_1 do
      end
    end

    Insight::Dsl::ExperimentDsl.new e, &Insight::Experiment.definitions[e.uid]
  end

end
