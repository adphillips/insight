Insight::Experiment.define :simple_product_page do

  description 'Reducing the amount of items on the page'

  control do
    title 'The current number of items'

    ## Runs before each event
    ##   :join - when a user is placed in this group
    ##   :convert - when a user converts
    ##   :test - when a test is performed and the user is in this group
    #before :event do
    #end

    #after :event do
    #end
  end

  alternative :less_items do
    #title 'Less items'

    #setup do
      #@body_classes << 'new_sidebar'
      #@partial = 'sidebar/new'
    #end
  end

  alternative :more_items do

  end

  #sample_every 20
  #show_after 10

end
