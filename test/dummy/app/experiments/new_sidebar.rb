Insight::Experiment.define :new_sidebar do

  title 'New sidebar'
  description 'Test if the new sidebar is better than the old'

  control do
    title 'The boring old sidebar'

    ## Runs before each event
    ##   :join - when a user is placed in this group
    ##   :convert - when a user converts
    ##   :test - when a test is performed and the user is in this group
    #before :event do
    #end

    #after :event do
    #end
  end

  alternative :new_sidebar do
    title 'The funky new sidebar'

    #setup do
      #@body_classes << 'new_sidebar'
      #@partial = 'sidebar/new'
    #end
  end

  #sample_every 20
  #show_after 10

end
