class StaticController < ApplicationController
  def home
    for_test :new_sidebar do |alternative|
      case alternative.uid
        when :new_sidebar
          @test1 = 'set to alternative'
        when :control
          @test1 = 'set to control'
      end
    end

    for_test :simple_product_page => :control do
      @test2 = 'normal product page'
    end

    for_test :simple_product_page => :less_items do
      @test2 = 'other product page'
    end
  end

  def convert_sidebar
    convert! :new_sidebar
  end

  def convert_product_page
    convert! :simple_product_page
  end

  def convert_both
    convert! [:new_sidebar, :simple_product_page]
  end
end
