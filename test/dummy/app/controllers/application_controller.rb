class ApplicationController < ActionController::Base
  protect_from_forgery

  # before_filter :setup_ab_tests

  def setup_ab_tests
    ab_test :new_sidebar

    # This should:
    #   Define the test in the db if it isn't already
    #   Put the user in a test group that persists for the entire session
    #   Execute the code specified in the setup block for either the control or alternative group

    ab_test :new_sidebar do |group|
      case group
        when :control
        when :alternative
      end
    end
  end

  def convert_ab_test
    convert! :new_sidebar
    # This should:
    #   Increment the db data
    #   Sample the db data if necessary
  end
end
