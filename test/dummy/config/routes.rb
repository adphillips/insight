Rails.application.routes.draw do

  mount Insight::Engine => "/insight"

  match 'home' => 'static#home'
  match 'convert_product_page' => 'static#convert_product_page'
  match 'convert_sidebar' => 'static#convert_sidebar'
  match 'convert_both' => 'static#convert_both'

end
