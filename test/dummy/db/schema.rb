# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120824210108) do

  create_table "insight_experiments", :force => true do |t|
    t.string   "uid"
    t.string   "title"
    t.integer  "control_participants",     :default => 0
    t.integer  "control_conversions",      :default => 0
    t.integer  "alternative_participants", :default => 0
    t.integer  "alternative_conversions"
    t.boolean  "started",                  :default => false
    t.boolean  "complete",                 :default => false
    t.datetime "completed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "participants",             :default => 0
    t.text     "description"
  end

  add_index "insight_experiments", ["complete"], :name => "index_insight_experiments_on_complete"
  add_index "insight_experiments", ["completed_at"], :name => "index_insight_experiments_on_completed_at"
  add_index "insight_experiments", ["started"], :name => "index_insight_experiments_on_started"
  add_index "insight_experiments", ["uid"], :name => "index_insight_experiments_on_uid", :unique => true

  create_table "insight_samples", :force => true do |t|
    t.integer  "experiment_id"
    t.integer  "control_participants",        :default => 0
    t.integer  "control_conversions",         :default => 0
    t.integer  "control_conversion_rate",     :default => 0
    t.integer  "alternative_participants",    :default => 0
    t.integer  "alternative_conversions",     :default => 0
    t.decimal  "alternative_conversion_rate", :default => 0.0
    t.decimal  "z_score",                     :default => 0.0
    t.decimal  "p_value",                     :default => 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "participants",                :default => 0
    t.text     "data"
  end

  add_index "insight_samples", ["experiment_id"], :name => "index_insight_samples_on_insight_experiment_id"
  add_index "insight_samples", ["p_value"], :name => "index_insight_samples_on_p_value"

end
