# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

require File.expand_path("../dummy/config/environment.rb",  __FILE__)
require "rails/test_help"
require 'shoulda'
require 'factory_girl'
require 'ruby-debug'

require 'test_console'
include TestConsole::Output

begin; require 'turn'; rescue LoadError; end

Rails.backtrace_cleaner.remove_silencers!

Insight.cache.clear

# Load factories
Dir["#{File.dirname(__FILE__)}/factories/**/*.rb"].each { |f| require f }
# Load support files
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

