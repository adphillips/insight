class ActiveSupport::TestCase
  def assert_false value
    assert_equal false, value
  end

  def self.should_be_false &block
    should 'be false' do
      assert_false instance_eval(&block) if block_given?
    end
  end

  def self.should_be_true &block
    should 'be true' do
      assert instance_eval(&block) if block_given?
    end
  end
end
