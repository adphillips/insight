class ActiveSupport::TestCase
  def setup_split_test uid=nil, opts={}
    Insight.cache.clear if opts[:clear_cache]

    uid ||= :a_split_test
    @experiment ||= experiment_with_alternatives uid
    @experiment
  end

  def teardown_split_test uid, opts
    Insight::Experiment.destroy_by_uid uid
    Insight.cache.clear if opts[:clear_cache]
  end

  def self.should_have_sampled values
    should "have sampled #{values.inspect}" do
      assert_not_nil @experiment.samples.last
      assert_equal values, @experiment.samples.last.data
    end
  end

  def self.with_split_test uid=nil, opts={}, &block
    uid ||= :a_split_test
    opts[:clear_cache] ||= true

    # These methods are nested inside the definition of with_split_test
    # This is so that they are only defined within the context of a split test and can therefore assume the presence of an experiment

    def self.for_alternative alt, opts={}, &block
      context "for alternative #{alt.to_s}" do
        setup do
          opts[:insight_id] ||= @controller.insight_id if @controller
          raise 'Test helper error: for_alternative missing insight_id' unless opts[:insight_id]
          cache_id = ['split_tests', @experiment.uid, opts[:insight_id], :participant]
          Insight.cache.write(Insight.cache_key_for(cache_id), alt.to_sym)
          @alternative_uid = alt
        end

        teardown do
          cache_id = ['split_tests', @experiment.uid, opts[:insight_id], :participant]
          Insight.cache.write(Insight.cache_key_for(cache_id), nil)
          @alternative_uid = nil
        end

        merge_block(&block)
      end
    end

    def self.with_test_data values={}, &block
      values = [values] unless values.kind_of? Array
      # TestConsole.out values, :yellow
      values.each do |value|
        context "with test data #{value.inspect}" do
          setup do
            value.each do |vkey, vval|
              @experiment.alternative(vkey).participants = vval[:participants] if vval[:participants]
              @experiment.alternative(vkey).conversions = vval[:conversions] if vval[:conversions]
            end
          end

          teardown do
            value.each do |vkey, vval|
              @experiment.alternative(vkey).participants = vval[:participants] if vval[:participants]
              @experiment.alternative(vkey).conversions = vval[:conversions] if vval[:conversions]
            end
          end

          merge_block(&block)
        end
      end
    end

    context "with split test #{uid}" do
      setup do
        setup_split_test uid, opts
      end

      teardown do
        teardown_split_test uid, opts
      end

      merge_block(&block)
    end
  end

  def experiment_with_alternatives uid
    Insight::Experiment.define uid do
      control do
      end

      alternative :alt_1 do
      end
    end

    Insight::Experiment.find_or_create_by_uid uid
  end
end

