require 'test_helper'

class InsightTest < ActiveSupport::TestCase
  context '.cache' do
    context 'by default' do
      should 'return the Rails cache' do
        assert_equal Rails.cache, Insight.cache
      end
    end

    context 'once set to an alternative cache store' do
      setup do
        @new_cache = Insight.cache = ActiveSupport::Cache::FileStore.new("#{Rails.root.to_s}/tmp/cache/alternative/")
      end

      teardown do
        Insight.cache = Rails.cache
      end

      should 'return the new cache' do
        assert_equal @new_cache, Insight.cache
      end
    end
  end

  context '.cache_key_for' do
    context 'when passed an identifier' do
      setup do
        @identity = '123'
      end

      should 'return the cache key' do
        assert_equal "#{@identity}", Insight.cache_key_for(@identity)
      end
    end

    context 'when passed an array' do
      setup do
        @identity = ['abc', 'def']
      end

      should 'return the cache key' do
        assert_equal "abc/def", Insight.cache_key_for(@identity)
      end
    end
  end
end
