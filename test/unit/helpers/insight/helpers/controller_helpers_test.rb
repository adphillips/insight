require 'test_helper'

class Insight::Helpers::ControllerHelpersTest < ActionController::TestCase
  tests ApplicationController

  setup do
    #@controller.send :include, Insight::Helpers::ControllerHelpers
  end

  context '#alternative_for' do
    with_split_test :a_test do
      for_alternative :control do
        should 'return the control' do
          assert_equal @controller.alternative_for(:a_test), @experiment.control
        end
      end

      for_alternative :alt_1 do
        should 'return the alternative' do
          assert_equal @controller.alternative_for(:a_test), @experiment.alternative(:alt_1)
        end
      end
    end
  end

  context '#convert!' do
    with_split_test :a_test do
      context 'when passed a test id as a symbol' do
        context 'when the insight_id is not a participant in the specified test' do
          should 'not increment the number of conversions in the specified test' do
            assert_difference '@experiment.conversions', 0 do
              @controller.convert! :a_test
            end
          end

          should 'not increment the number of conversions for each of the alternatives or the control' do
            assert_difference '@experiment.control.conversions', 0 do
              @controller.convert! :a_test
            end

            @experiment.alternatives.each do |alt|
              assert_difference 'alt[1].conversions', 0 do
                @controller.convert! :a_test
              end
            end
          end
        end

        context 'when the insight_id is a participant in the specified test' do
          setup do
            @experiment.join @controller.insight_id
          end

          teardown do
            @experiment.leave @controller.insight_id
          end

          should 'increment the number of conversions for the test' do
            assert_difference '@experiment.conversions', 1 do
              @controller.convert! :a_test
            end
          end

          should 'increment the number of conversions for the current alternative' do
            chosen_alternative = @controller.alternative_for :a_test
            assert_difference 'chosen_alternative.conversions', 1 do
              @controller.convert! :a_test
            end
          end
        end
      end

      context 'when passed an array of test ids' do
        setup do
          @experiment1 = @experiment
        end

        with_split_test :another_test, :clear_cache => false do
          setup do
            @experiment.join @controller.insight_id
          end

          teardown do
            @experiment.leave @controller.insight_id
          end

          should 'increment the number of conversions for each test' do
            assert_difference '@experiment.conversions', 1 do
              assert_difference '@experiment1.conversions', 1 do
                @controller.convert! [:a_test, :another_test]
              end
            end
          end
        end
      end
    end
  end

  context '#for_test' do
    with_split_test :a_test do
      context 'when passed a hash containing test => alternative' do
        setup do
          @test_param = {:a_test => :alt_1}
        end

        context 'and a block' do
          for_alternative :control do
            should 'not execute the block' do
              @controller.for_test @test_param do
                @something = 'set'
              end

              assert_equal nil, @something
            end
          end

          for_alternative :alt_1 do
            should 'execute the block' do
              @controller.for_test @test_param do
                @something = 'set'
              end

              assert_equal 'set', @something
            end
          end
        end
      end

      context 'when passed a symbol' do
        setup do
          @test_param = :a_test
        end

        context 'and a block' do
          for_alternative :control do
            should 'execute the block passing the alternative as a parameter' do
              @controller.for_test :a_test do |alternative|
                # debugger
                @alt_value = alternative
              end

              assert_not_nil @alt_value
              assert_equal @alt_value.uid, :control
            end
          end

          for_alternative :alt_1 do
            should 'execute the block passing the alternative as a parameter' do
              @controller.for_test :a_test do |alternative|
                @alt_value = alternative
              end

              assert_not_nil @alt_value
              # debugger
              assert_equal @alt_value.uid, :alt_1
            end
          end
        end
      end
    end
  end

  context '#insight_id' do
    context 'by default' do
      should 'return the session id' do
        assert request.session_options[:id], @controller.insight_id
      end
    end
  end

  context '#split_test' do
    context 'when passed a test uid' do
      context 'when the experiment does\'t have a record in the db' do
        setup do
          Insight::Experiment.destroy_by_uid :a_new_test
        end
        teardown do
          Insight::Experiment.destroy_by_uid :a_new_test
        end
        should 'create a record for the experiment' do
          assert_difference('Insight::Experiment.count', 1) do
            @controller.split_test :a_new_test
          end
        end
      end

      context 'when the experiment has a record in the db' do
        with_split_test :a_new_test do
          should 'not create a new record' do
            assert_equal 1, Insight::Experiment.where(:uid => :a_new_test).size
            assert_difference('Insight::Experiment.count', 0) do
              @controller.split_test :a_new_test
            end
          end

          should 'return the chosen alternative' do
            assert_equal @controller.split_test(:a_new_test).uid, @experiment.alternative_for(@controller.insight_id).uid
          end


          should 'increment the number of participants in the test' do
            assert_difference('@experiment.reload.participants', 1) do
              @controller.split_test :a_new_test
            end
          end

          should 'add to the @split_tests instance variable' do
            alt = @controller.split_test :a_new_test
            assert_not_nil @controller.instance_variable_get(:@split_tests)
            assert_not_nil @controller.instance_variable_get(:@split_tests)[:a_new_test]
            assert_equal alt.uid, @controller.instance_variable_get(:@split_tests)[:a_new_test].uid
          end
        end
      end
    end
  end

end
