require 'test_helper'

module Insight
  class ApplicationHelperTest < ActionView::TestCase

    context '#colored_change' do
      context 'when nil is passed' do
        should 'return an empty string' do
          assert_equal '', colored_change(nil)
        end
      end

      context 'when the value passed is > 0' do
        should 'return the value rounded to the specified precision wrapped in an "upward_change" class' do
          assert_equal '<span class="upward_change">0.5</span>', colored_change(0.5234, 1)
        end
      end

      context 'when the value passed is < 0' do
        should 'return the value rounded to the specified precision wrapped in an "downward_change" class' do
          assert_equal '<span class="downward_change">-0.52</span>', colored_change(-0.5234)
        end
      end
    end
  end
end
