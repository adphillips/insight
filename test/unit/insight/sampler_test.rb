require 'test_helper'

module Insight
  class SamplerTest < ActiveSupport::TestCase

    def self.data set_id, base_value
      case set_id
      when :set_1
        {
          :control => {:participants => ((base_value - 1) / 2).to_i, :conversions => (base_value / 4).to_i},
          :alt_1 => {:participants => (base_value/ 2).to_i, :conversions => (base_value / 4).to_i},
        }
      when :set_2
        {
          :control => {:participants => base_value, :conversions => 0},
          :alt_1 => {:participants => 5, :conversions => 0}
        }
      end
    end

    with_split_test :a_test do
      context 'when the experiment has a sample rate set' do
        context 'to every 5 participants' do
          setup do
            @experiment.stubs(:sample_rate).returns(:participants => 5)
          end

          with_test_data data(:set_1, 5) do
            context  'when someone joins the test' do
              setup do
                @experiment.join('someone', :control)
              end

              should_have_sampled({
                :participants => 5,
                :conversions => 2,
                :alternatives => {
                  :control => {
                    :participants => 3,
                    :conversions => 1
                  },
                  :alt_1 => {
                    :participants => 2,
                    :conversions => 1
                  }
                }
              })
            end
          end

          with_test_data data(:set_1, 10) do
            context  'when someone joins the test' do
              setup do
                @experiment.join('someone', :control)
              end

              should_have_sampled({
                :participants => 10,
                :conversions => 4,
                :alternatives => {
                  :control => {
                    :participants => 5,
                    :conversions => 2
                  },
                  :alt_1 => {
                    :participants => 5,
                    :conversions => 2
                  }
                }
              })
            end
          end

          teardown do
            @experiment.leave('someone')
          end

        end
      end
    end

    context '#sample' do
      with_split_test :a_test do
        should 'return an new instance of Insight::Sample' do
          assert @experiment.sample.kind_of?(Insight::Sample)
        end

        should 'create a new record' do
          assert_difference '@experiment.samples.reload.size', 1 do
            @experiment.sample
          end
        end

        context 'the returned sample' do
          should 'have the experiment set' do
            assert_equal @experiment, @experiment.sample.experiment
          end

          context 'when the experiment has participants and conversions' do
            setup do
              convert = true
              for x in 1..10
                convert = !convert if x.modulo(2) == 0
                x = "id-#{x}"
                @experiment.join x
                @experiment.convert x if convert
              end
            end

            should 'have its data set to a hash of the current participant and conversion counts' do
              assert_equal({
                :participants => 10,
                :conversions => 5,
                :alternatives => {
                  :control => {
                    :participants => 5,
                    :conversions => 3
                  },
                  :alt_1 => {
                    :participants => 5,
                    :conversions => 2
                  }
                }
              }, @experiment.sample.data)
            end
          end
        end
      end
    end

    context '#sample_required?' do
      with_split_test :a_test do
        context 'when the experiment has a sample rate set' do
          context 'to every 5 participants' do
            setup do
              @experiment.stubs(:sample_rate).returns(:participants => 5)
            end

            with_test_data [data(:set_2, 0), data(:set_2, 5), data(:set_2, 10)] do

              should_be_true{@experiment.sample_required?}
            end

            with_test_data [data(:set_2, 1), data(:set_2, 2), data(:set_2, 3), data(:set_2, 4), data(:set_2, 6)] do
              should_be_false{@experiment.sample_required?}
            end

            teardown do
              @experiment.leave('someone')
            end

          end
        end
      end
    end
  end
end
