require 'test_helper'

module Insight
  class ExperimentTest < ActiveSupport::TestCase

    context 'after_load' do
      context 'when the experiment has a definition' do
        setup do
          @experiment = Insight::Experiment.define :a_test do
            description 'some test'
          end
        end

        teardown do
          Insight::Experiment.destroy_by_uid :a_test
        end

        should 'decorate the experiment with the data from the definition' do
          assert_equal 'some test', @experiment.description
          id = @experiment.id
          exp = Insight::Experiment.find(id)
          assert_equal 'some test', exp.description
        end
      end
    end

    context '#load_id' do
      context 'when there isnt a record with the same uid' do
        should 'not load the records id' do
          @experiment = Factory.build(:experiment)
          @experiment.uid = :new_test
          assert_nil @experiment.id
          @experiment.load_id
          assert_nil @experiment.id
        end
      end
      context 'when there is already a record with the same uid' do
        with_split_test :a_test do
          should 'load the existing records id' do
            @new_experiment = Factory.build(:experiment)
            @new_experiment.uid = :a_test
            @new_experiment.load_id
            assert_equal @experiment.id, @new_experiment.id
          end
        end
      end
    end

    context '#alternative' do
      with_split_test :a_test do
        context 'when passed :control' do
          should 'return the control alternative' do
            alt = @experiment.alternative :control
            assert alt.kind_of?(Insight::Alternative)
            assert_equal :control, alt.uid
          end
        end
        context 'when passed the uid of an alternative' do
          should 'return the specified alternative' do
            alt = @experiment.alternative :alt_1
            assert alt.kind_of?(Insight::Alternative)
            assert_equal :alt_1, alt.uid
          end
        end
      end
    end

    context '#alternatives' do
      with_split_test :a_test do
        should 'return the defined alternatives' do
          assert_equal 1, @experiment.alternatives.length
          assert @experiment.alternatives.has_key?(:alt_1)
          assert_equal :alt_1, @experiment.alternatives.first[0]
        end
      end
    end

    context '#alternative_for' do
      with_split_test :some_test do
        context 'when passed an identity' do
          setup do
            @identity = 'someid'
          end
          context 'and that identity doesnt have a chosen alternative' do
            setup do
              Insight.cache.write("split_tests/some_test/#{@identity}/participant", nil)
            end

            teardown do
              Insight.cache.delete "split_tests/some_test/#{@identity}/participant"
            end

            should 'return nil' do
              assert_nil @experiment.alternative_for(@identity)
            end
          end

          context 'and that identity has already been placed in a group' do
            setup do
              Insight.cache.write "split_tests/some_test/#{@identity}/participant", @experiment.control.uid
            end

            teardown do
              Insight.cache.delete "split_tests/some_test/#{@identity}/participant"
            end

            should 'return the same group' do
              assert_equal :control, @experiment.alternative_for(@identity).uid
            end

            should 'not increment the participants in the experiment' do
              assert_difference '@experiment.participants', 0 do
                @experiment.alternative_for(@identity)
              end
            end

            should 'not increment the participants in the alternative' do
              counts = {}
              @experiment.alternatives.each{|c| counts[c[0]] = c[1].participants}
              counts[:control] = @experiment.control.participants
              alt = @experiment.alternative_for(@identity)
              assert_equal counts[alt.uid], alt.participants
            end
          end
        end
      end
    end

    context '#cache_key_for' do
      with_split_test do
        context 'when passed an identifier' do
          setup do
            @identity = '123'
          end

          should 'return the cache key' do
            assert_equal "split_tests/#{@experiment.uid}/#{@identity}", @experiment.cache_key_for(@identity)
          end
        end

        context 'when passed an array' do
          setup do
            @identity = ['abc', 'def']
          end

          should 'return the cache key' do
            assert_equal "split_tests/#{@experiment.uid}/abc/def", @experiment.cache_key_for(@identity)
          end
        end
      end
    end

    context '#convert' do
      with_split_test :a_test do
        context 'when passed a participant identifier' do
          setup do
            @id = 'some_one'
          end

          context 'when the identifier is not a participant in the test' do
            setup do
              @experiment.leave @id
            end

            teardown do
              @experiment.leave @id
            end

            should 'not mark the participant as converted' do
              @experiment.convert @id
              assert_false @experiment.converted?(@id)
            end

            should 'not increase the number of conversions for the experiment' do
              assert_difference '@experiment.conversions', 0 do
                @experiment.convert @id
              end
            end
          end

          context 'when the identifier is a participant in the test' do
            setup do
              @experiment.join @id
            end

            context 'when the participant has not already converted' do
              should 'mark the participant as converted' do
                @experiment.convert @id
                assert @experiment.converted?(@id)
              end

              should 'increase the number of conversions for the experiment' do
                assert_difference '@experiment.conversions', 1 do
                  @experiment.convert @id
                end
              end

              should 'increase the number of the conversions for the alternative' do
                assert_difference '@experiment.alternative_for(@id).conversions', 1 do
                  @experiment.convert @id
                end
              end
            end

            context 'when the participant has already converted' do
              setup do
                @experiment.convert @id
              end

              should 'still have the participant as converted' do
                @experiment.convert @id
                assert @experiment.converted?(@id)
              end

              should 'not increase the number of conversions for the experiment' do
                assert_difference '@experiment.conversions', 0 do
                  @experiment.convert @id
                end
              end

              should 'not increase the number of the conversions for the alternative' do
                assert_difference '@experiment.alternative_for(@id).conversions', 0 do
                  @experiment.convert @id
                end
              end
            end
          end
        end
      end
    end

    context '#converted?' do
      with_split_test :a_test do
        context 'when passed an id' do
          setup do
            @id = '123'
          end

          should 'return false' do
            assert_equal @experiment.converted?(@id), false
          end

          context 'when the experiment has been converted for the participant' do
            setup do
              @experiment.join @id
              @experiment.convert @id
            end

            should 'return true' do
              assert @experiment.converted?(@id)
            end
          end
        end
      end
    end

    context '#definition' do
      context 'when the experiment has already been defined' do
        with_split_test :defined_test do
          should 'return the definition' do
            assert_not_nil @experiment.definition
            assert_equal Insight::Experiment.definitions[@experiment.uid], @experiment.definition
          end
        end
      end

      context 'when the experiment hasnt already been setup' do
        should 'return nil' do
          @experiment = Factory.build(:experiment, :uid => :undefined_test)
          assert_nil @experiment.definition
        end
      end
    end

    context '#definition_path' do
      with_split_test :a_test do
        should 'return the path to the expected definition' do
          assert_equal "#{Rails.root.to_s}/app/experiments/a_test.rb", @experiment.definition_path
        end
      end
    end

    context '#has_record?' do
      context 'when the test has a record in the db with the same uid' do
        with_split_test :something_else do
          should 'return true' do
            assert @experiment.has_record?
          end
        end
      end

      context 'when the test doesnt have a db record' do
        setup do
          @experiment = Factory.build(:experiment, :uid => :something_else)
        end

        should 'return false' do
          assert_equal false, @experiment.has_record?
        end
      end
    end

    context '#join' do
      setup do
        @identity = '123'
      end

      with_split_test :a_test do
        context 'when the specified identity is not already a participant in the test' do
          teardown do
            @experiment.leave @identity
          end

          should 'set the identity as a participant in the test' do
            @experiment.join @identity
            assert @experiment.participant?(@identity)
          end

          should 'increment the number of participants in the test' do
            assert_difference '@experiment.participants', 1 do
              @experiment.join @identity
            end
          end

          should 'assign an alternative to the identity' do
            @experiment.join @identity
            assert_not_nil @experiment.alternative_for(@identity)
          end

          should 'return the chosen alternative' do
            ret = @experiment.join @identity
            assert_equal @experiment.alternative_for(@identity), ret
          end

          should 'increment the number of participants for the alternative' do
            counts = {}
            @experiment.alternatives.each{|c| counts[c[0]] = c[1].participants}
            counts[:control] = @experiment.control.participants
            alt = @experiment.join(@identity)
            assert_equal counts[alt.uid] + 1, alt.participants
          end

          context 'when a alternative id is passed' do
            should 'set the participant to the specified control group' do
              assert_equal @experiment.alternative(:control), @experiment.join('someone', :control)
              @experiment.leave('someone')

              assert_equal @experiment.alternative(:alt_1), @experiment.join('someone', :alt_1)
              @experiment.leave('someone')
            end
          end
        end

        context 'when the specified identity is already a participant in the test' do
          setup do
            @alternative = @experiment.join @identity
          end

          teardown do
            @experiment.leave @identity
          end

          should 'keep the identity as a participant in the test' do
            @experiment.join @identity
            assert @experiment.participant?(@identity)
          end

          should 'not increment the number of participants in the test' do
            assert_difference '@experiment.participants', 0 do
              @experiment.join @identity
            end
          end

          should 'return the alternative assigned to the identity' do
            assert_equal @alternative, @experiment.join(@identity)
          end

          should 'not increment the number of participants for the alternative' do
            counts = {}
            @experiment.alternatives.each{|c| counts[c[0]] = c[1].participants}
            counts[:control] = @experiment.control.participants
            alt = @experiment.join(@identity)
            assert_equal counts[alt.uid], alt.participants
          end

        end
      end
    end

    context '#leader' do
      with_split_test :a_test do
        context 'when the control has a higher conversion rate' do
          setup do
            @experiment.control.participants = 10
            @experiment.control.conversions = 10
          end

          should 'return the control' do
            assert_equal @experiment.control, @experiment.leader
          end
        end

        context 'when the alternative has a higher conversion rate' do
          setup do
            @experiment.alternatives[:alt_1].participants = 10
            @experiment.alternatives[:alt_1].conversions = 10
          end

          should 'return the alternative' do
            assert_equal @experiment.alternatives[:alt_1], @experiment.leader
          end
        end
      end
    end

    context '#leave' do
      with_split_test :a_test do
        context 'when passed an identifier' do
          setup do
            @identity = '123'
          end

          context 'when the identity is alread a participant in the test' do
            setup do
              @experiment.join @identity
            end

            should 'remove the participant from the group' do
              @experiment.leave @identity
              assert_false @experiment.participant?(@identity)
            end
          end
        end
      end
    end

    context '#next_alternative' do
      with_split_test do
        context 'when the experiment has more participants in the control group' do
          setup do
            @experiment.control.participants = 10
          end

          should 'choose the alternative and return its uid' do
            assert_equal :alt_1, @experiment.next_alternative
          end
        end

        context 'when the experiment has more participants in the alternative group' do
          setup do
            @experiment.alternative(:alt_1).participants = 10
          end

          should 'choose the control and return :control' do
            assert_equal :control, @experiment.next_alternative
          end
        end
      end
    end

    context '#participant?' do
      with_split_test :a_test do
        setup do
          @identity = 'a_participant'
        end

        setup do
          @experiment.leave @identity
        end

        context 'when the specified identity has not been added to the experiment' do
          should 'return false' do
            assert_false @experiment.participant?(@identity)
          end
        end

        context 'when the specified identity has been added to the experiment' do
          setup do
            @experiment.join @identity
          end

          should 'return true' do
            assert @experiment.participant?(@identity)
          end
        end
      end
    end

    context '#participants' do
      with_split_test do
        # TODO replace this when sampling is working
        should 'return 0' do
          assert_equal(0, @experiment.participants)
        end
      end
    end

    context '.define' do

      context 'when passed a symbol' do

        should 'return a test instance' do
          test = Insight::Experiment.define :a_sample_test

          assert_equal :a_sample_test, test.uid
          assert_equal 'A sample test', test.title

          Insight::Experiment.undefine :a_sample_test
        end

        context 'and a block' do
          context 'containing a title' do
            should 'return a test instance with the title set' do
              test = Insight::Experiment.define :a_sample_test do
                title 'A different name'
              end

              assert_equal :a_sample_test, test.uid
              assert_equal 'A different name', test.title

              Insight::Experiment.undefine :a_sample_test
            end
          end

          context 'containing a description' do
            should 'return a test instance with the description set' do
              test = Insight::Experiment.define :a_sample_test do
                description 'A different description'
              end

              assert_equal 'A different description', test.description

              Insight::Experiment.undefine :a_sample_test
            end
          end

          context 'containing an alternative' do
            should 'return a test instance with an alternative' do
              test = Insight::Experiment.define :a_sample_test do
                alternative :new_version do
                end
              end

              assert_equal 1, test.alternatives.length
              assert_equal :new_version, test.alternatives[:new_version].uid
              assert_equal 'New version', test.alternatives[:new_version].title
              assert_equal false, test.alternatives[:new_version].control?
              assert_equal test, test.alternatives[:new_version].experiment

              Insight::Experiment.undefine :a_sample_test
            end
          end

          context 'containing a control' do
            should 'return a test instance with an alternative' do
              test = Insight::Experiment.define :a_sample_test do
                control do
                end
              end

              assert_not_nil test.control
              assert_kind_of Insight::Alternative, test.control
              assert_equal :control, test.control.uid
              assert_equal 'Control', test.control.title
              assert test.control.control?
              assert_equal test, test.control.experiment

              Insight::Experiment.undefine :a_sample_test
            end
          end
        end
      end
    end

    context '.destroy_by_uid' do
      context 'when passed a defined test uid' do
        setup do
          @experiment = Factory.create(:experiment, :uid => :a_new_test)
        end

        should 'delete the record' do
          Insight::Experiment.destroy_by_uid :a_new_test
          assert_equal 0, Insight::Experiment.where(:uid => :a_new_test).count
        end

        should 'undefine the experiment' do
          Insight::Experiment.destroy_by_uid :a_new_test
          assert_nil Insight::Experiment.defined[:a_new_test]
        end
      end
    end

    context '.find_or_create_by_uid' do
      context 'when the record exists' do
        setup do
          @experiment = Factory.create(:experiment, :uid => 'a_test')
        end

        teardown do
          @experiment.delete
        end

        should 'not create a record' do
          assert_difference 'Insight::Experiment.count', 0 do
            Insight::Experiment.find_or_create_by_uid :a_test
          end
        end
      end

      context 'when the record doesnt exist' do
        setup do
          Insight::Experiment.delete_all
        end
        should 'create the record' do
          # debugger
          assert_difference 'Insight::Experiment.count', 1 do
            Insight::Experiment.find_or_create_by_uid :a_new_test
          end
        end
      end

      context 'when the test has a definition' do
        setup do
          Insight::Experiment.define :some_new_test do
            title 'A new test'
          end
        end

        teardown do
          Insight::Experiment.undefine :some_new_test
        end

        should 'load the definition' do
          experiment = Insight::Experiment.find_or_create_by_uid :some_new_test
          assert_equal 'A new test', experiment.title
        end
      end
    end

    context '.undefine' do
      context 'when passed a defined test uid' do
        setup do
          @experiment = Insight::Experiment.define :a_test do

          end
        end

        teardown do
          Insight::Experiment.undefine :a_test
        end

        should 'remove the experiments definition' do
          Insight::Experiment.undefine :a_test
          assert_nil Insight::Experiment.defined[:a_test]
          assert_nil Insight::Experiment.definitions[:a_test]
        end
      end
    end

    context '#conversions_cache_key' do
      with_split_test :a_test do
        should 'return the cache key' do
          assert_equal @experiment.send(:conversions_cache_key), "split_tests/#{@experiment.uid}/conversions"
        end
      end
    end

    context '#participants_cache_key' do
      with_split_test :a_test do
        should 'return the cache key' do
          assert_equal @experiment.send(:participants_cache_key), "split_tests/#{@experiment.uid}/participants"
        end
      end
    end
  end
end
