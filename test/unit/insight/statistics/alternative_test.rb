require 'test_helper'

module Insight
  module Statistics
    class AlternativeTest < ActiveSupport::TestCase
      with_split_test :a_test do
        context 'when the alternative has conversion data' do
          setup do
            @alternative = @experiment.alternative :alt_1
            @alternative.participants = 1000
            @alternative.conversions = 700
          end

          should 'have the correct #conversion_rate' do
            assert_equal 0.70, @alternative.conversion_rate
          end

          context 'when the alternative is a control' do
            setup do
              @control = @experiment.control
            end

            context '#confidence_level' do
              should 'return 0.0' do
                assert_equal 0.0, @control.confidence_level
              end
            end

            context '#conversion_change' do
              should 'return 0.0' do
                assert_equal 0.0, @control.conversion_change
              end
            end

            context '#z_score' do
              should 'return 0.0' do
                assert_equal 0.0, @control.z_score
              end
            end
          end

          context 'when the alternative is not a control' do
            context 'and the experiment control alternative has a conversion rate' do
              setup do
                @experiment.control.participants = 1000
                @experiment.control.conversions = 500
              end

              context '#confidence_level' do
                should 'return the confidence level' do
                  assert_equal 0.99995, @alternative.confidence_level.round(5)
                end
              end

              context '#conversion_change' do
                should 'return the percentage change in conversion rate' do
                  assert_equal 0.4, @alternative.conversion_change.round(1)
                end
              end

              context '#z_score' do
                should 'return the z_score' do
                  assert_equal 9.325, @alternative.z_score.round(3)
                end
              end
            end
          end
        end
      end
    end
  end
end

