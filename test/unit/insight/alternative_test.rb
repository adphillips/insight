require 'test_helper'

module Insight
  class AlternativeTest < ActiveSupport::TestCase

    context '#experiment' do
      context 'when the experiment_uid has not been set' do
        should 'return nil' do
          al = Insight::Alternative.new :something

          assert_nil al.experiment
        end
      end
    end

    context '#experiment=' do
      setup do
        @alternative = Insight::Alternative.new :something
        @experiment = experiment_with_alternatives :an_experiment
      end

      teardown do
        Insight::Experiment.destroy_by_uid :an_experiment
      end

      context 'when an experiment is passed' do
        should 'set @experiment' do
          @alternative.experiment = @experiment
          assert_equal @alternative.experiment, @experiment
        end

        should 'set @experiment_uid' do
          @alternative.experiment = @experiment
          assert_equal @alternative.experiment, @experiment
        end

        should 'return the experiment' do
          ret = @alternative.experiment = @experiment
          assert_equal ret, @experiment
        end
      end
    end

    context '#conversions' do
      with_split_test :a_test do
        setup do
          @id = '123'
          @experiment.join @id
          @alt = @experiment.alternative_for(@id)
        end

        should 'return 0' do
          assert_equal 0, @alt.conversions
        end
      end
    end

    context '#cache_key_for' do
      with_split_test :a_test do
        setup do
          @id = '123'
          @experiment.join @id
          @alt = @experiment.alternative_for(@id)
        end

        should 'return the cache key' do
          assert_equal @alt.send(:cache_key_for, [:some, :thing]), @alt.experiment.cache_key_for([:alternatives, @alt.uid, :some, :thing])
        end
      end
    end

    context '#leader' do
      with_split_test :a_test do
        context 'when the alternative is the leader in the experiment' do
          setup do
            @experiment.stubs(:leader).returns(@experiment.alternatives[:alt_1])
            @alternative = @experiment.alternatives[:alt_1]
          end

          should 'return true' do
            assert @alternative.leader?
          end
        end

        context 'when the alternative is not the leader in the experiment' do
          setup do
            @experiment.stubs(:leader).returns(@experiment.control)
            @alternative = @experiment.alternatives[:alt_1]
          end

          should 'return false' do
            assert_equal false, @alternative.leader?
          end
        end
      end
    end

    context '#participants' do
      with_split_test :a_test do
        setup do
          @id = '123'
          @experiment.join @id
          @alt = @experiment.alternative_for(@id)
        end

        should 'return 1' do
          assert_equal 1, @alt.participants
        end
      end
    end
  end
end
