require 'test_helper'

class Insight::DashboardControllerTest < ActionController::TestCase

  setup do
    @routes = Insight::Engine.routes
  end

  context '#home' do
    should 'work' do
      get :home
      assert_response :success
    end

    context 'when there are experiments' do
      with_split_test :a_test do
        should 'assign the experiments to @experiments' do
          get :home
          assert assigns(:experiments).include? @experiment
        end
      end
    end
  end

end
